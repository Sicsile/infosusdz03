import {
  expect
} from "chai";
import {
  shallowMount
} from "@vue/test-utils";
import UnosBiljeske from "@/components/UnosBiljeske.vue";
import Login from "@/views/Login.vue";
import Forbidden from "@/views/Forbidden.vue";
import UnosIzostanka from "@/components/UnosIzostanaka.vue";


describe("UnosBiljeske.vue", () => {
  it("Komponenta sadži zadani tekst", () => {
    const wrapper = shallowMount(UnosBiljeske);
    expect(wrapper.text()).to.include('Unos bilješkeŠifraJmbagOpisUnesi');
  });

  it("Komponenta nema niti jedan h2 element", () => {
    const wrapper = shallowMount(UnosBiljeske);
    const div = wrapper.find('h2');
    expect(div.exists()).equal(false);
  });

  it('Provjeri postoji li element h1 i njegov sadržaj mora biti jednak zadanom unosu', () => {
    let wrapper = shallowMount(UnosBiljeske);
    expect(wrapper.find('h1').text()).to.contain('Unos bilješke')
  });

  it('Provjeri postoji li gumb čiji je sadržaj jednak zadanom unosu', () => {
    let wrapper = shallowMount(UnosBiljeske);
    expect(wrapper.find('button').text()).to.contain('Unesi')
  });
})

describe("Login.vue", () => {
  it('View sadrži h1 element', () => {
    const wrapper = shallowMount(Login);
    const h1 = wrapper.find('h1');
    expect(h1.exists()).equal(true);
  })

  it('Ako je lozinka kriva mora se pokazati poruka o krivim kredencijalima', () => {
    const wrapper = shallowMount(Login, {
      data() {
        return {
          incorrect: true
        }
      }
    });
    const p = wrapper.find('p');
    expect(p.exists()).equal(true);
  })
})

describe('Forbidden.vue', () => {
  const tekstPoruke = "Zabranjen pristup";
  it('Provjera je li dobro ugrađen data u komponentu', () => {
    const wrapper = shallowMount(Forbidden, {
      data() {
        return {
          poruka: tekstPoruke,
        }
      }
    });
    expect(wrapper.find('h1').text()).equal(tekstPoruke)
  })
})

describe("UnosIzostanka.vue", () => {
  it('Provjerava sadrži li ime komponente tipa string', () => {
    expect(typeof UnosIzostanka.name).to.eql('string');
  });

  it('Provjerava je li funkcija submit tipa funkcija', () => {
    expect(typeof UnosIzostanka.methods.submit).to.eql('function');
  });

})