import {
  createRouter,
  createWebHistory
} from "vue-router";
import Login from "../views/Login.vue";
import Profesor from "../views/Profesor.vue";
import Ucenik from "../views/Ucenik.vue";
import Forbidden from "../views/Forbidden.vue";
import Profil from "../views/Profil.vue";
import OdabraniUcenik from "../views/OdabraniUcenik.vue"
import StatistikaProf from "../views/StatistikaProf.vue"
import StatistikaUc from "../views/StatistikaUc.vue"
import TablicaRazredi from "../components/TablicaRazredi.vue"
// import store from '../store/index.js';

const routes = [{
    path: "/",
    name: "Login",
    component: Login,
    meta: {
      requiresAuth: false
    }
  },
  {
    path: "/razredi",
    name: "TablicaRazredi",
    component: TablicaRazredi,
  },
  {
    path: "/profesor",
    name: "Profesor",
    component: Profesor,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/profesor/:id",
    name: "ProfilProf",
    component: Profil,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/profesor/odabrani-ucenik",
    name: "OdabraniUcenik",
    component: OdabraniUcenik,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/ucenik",
    name: "Ucenik",
    component: Ucenik,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/ucenik/:id",
    name: "ProfilUc",
    component: Profil,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/profesor/statistika",
    name: "StatistikaProf",
    component: StatistikaProf,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/ucenik/statistika",
    name: "StatistikaUc",
    component: StatistikaUc,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: "/forbidden",
    name: "Forbidden",
    component: Forbidden,
    meta: {
      requiresAuth: false
    }
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

//provjera neautoriziranog pristupa
router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    if (to.fullPath.startsWith('/profesor') && localStorage.getItem('user') !== null) {
      if (JSON.parse(localStorage.getItem('user')).uloga === 'prof') {
        next();
      } else {
        console.log("zabranjen pristup");
        next('/forbidden');
      }
    } else if (to.fullPath.startsWith('/ucenik') && localStorage.getItem('user') !== null) {
      if (JSON.parse(localStorage.getItem('user')).uloga === 'ucenik') {
        next();
      } else {
        console.log("zabranjen pristup");
        next('/forbidden');
      }
    }
  } else {
    next();
  }
})

export default router;