import { createStore } from "vuex";


export const mutations = {
  SETUSER: (state, user) => {
    state.user = user
  },
  LOGOUTUSER: (state) => {
    state.user = null,
    state.token = null,
    localStorage.clear()
  },
  SETTOKEN: (state, token) => {
    state.token = token
  },
  SETNAZIVRAZRED: (state, razred) => {
    state.nazivRazred.push(razred)
  },
  SETUCENICI: (state, ucenici) => {
    state.ucenici = state.ucenici.concat(ucenici)
  },
  SETODABRANIUCENICI: (state, odabraniUcenici) => {
    state.odabraniUcenici = odabraniUcenici
  },
  SETODABRANIRAZRED: (state, odabraniRazred) => {
    state.odabraniRazred = odabraniRazred
  },
  SETODABRANIPREDMET: (state, odabraniPredmet) => {
    state.odabraniPredmet = odabraniPredmet
  }, 
  SETSVIRAZREDI: (state, sviRazredi) => {
    state.sviRazredi = sviRazredi
  },
}


export default createStore({
  state: {
    user: localStorage.getItem('user') === null ? null : JSON.parse(localStorage.getItem('user')),
    token:  localStorage.getItem('token') === null ? null : JSON.parse(localStorage.getItem('token')),
    nazivRazred: [],
    ucenici: [],
    odabraniUcenici: [],
    odabraniRazred: "",
    odabraniPredmet: "",
    sviRazredi: []
  },
  mutations,
  actions: {
    setUserAction({commit}, payload){
      commit('SETUSER', payload)
    },
    setTokenAction({commit}, payload){
      commit('SETTOKEN', payload)
    },
    logoutUserAction({commit}){
      commit('LOGOUTUSER')
    },
    setNazivRazredAction({commit}, payload){
      commit('SETNAZIVRAZRED', payload);
    },
    setUceniciAction({commit}, payload){
      commit('SETUCENICI', payload);
    },
    setOdabraniUceniciAction({commit}, payload){
      commit('SETODABRANIUCENICI', payload);
    },    
    setOdabraniRazredAction({commit}, payload){
      commit('SETODABRANIRAZRED', payload);
    },
    setOdabraniPredmetAction({commit}, payload){
      commit('SETODABRANIPREDMET', payload);
    },
    setSviRazredi({commit}, payload){
      commit('SETSVIRAZREDI', payload);
    }    
  },
  getters: {
    getUser(store){
      return store.user
    },
    getToken(store){
      return store.token
    },
    getNazivRazred(store){
      return store.nazivRazred
    },
    getUcenici(store){
      return store.ucenici
    },
    getOdabraniUcenici(store){
      return store.odabraniUcenici
    },
    getOdabraniRazred(store){
      return store.odabraniRazred
    },
    getOdabraniPredmet(store){
      return store.odabraniPredmet
    },
    getSviRazredi(store){
      return store.sviRazredi
    },
    
  },
  modules: {},
  // plugins: [vuexLocal.plugin]
});
