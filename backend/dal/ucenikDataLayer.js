const db = require("../db/postgresql.js");


const ucenikDataLayerMethods = {
    getUcenikPodaci: async (jmbag) => {
        return await db.getUcenikPodaci(jmbag);
    },
    getUcenikPredmeti: async (jmbag) => {
        return await db.getUcenikPredmeti(jmbag);
    },
    getOcjeneUcenikPredmet: async (data) => {
        return await db.getOcjeneUcenikPredmet(data.id, data.sifpredmet);
    }
}

module.exports = ucenikDataLayerMethods;