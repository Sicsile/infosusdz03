const db = require("../db/postgresql.js");


const homeDataLayerMethods = {
    getNastavnik: async (password) => {
        return await db.getNastavnik(password)
    },
    getUcenik: async (password) => {
        return await db.getUcenik(password)
    },
    getRazredi: async () => {
        return await db.getRazredi()
    },
    createRazred: async(data) => {
        return await db.createRazred(data.sifrazred, data.naziv);
    },
    updateRazredNaziv: async(data) => {
        return await db.updateRazredNaziv(data.sifrazred, data.naziv);
    },
    deleteRazred: async(sifrazred) => {
        return await db.deleteRazred(sifrazred);
    },
    getMjesta: async () => {
        return await db.getMjesta();
    },
    createUcenik: async (data) => {
        return await db.createUcenik(data.ime, data.prezime, data.razred, data.jmbag, data.pbr)
    },
    getUcenikBiljeske: async (jmbag) => {
        return await db.getUcenikBiljeske(jmbag);
    },
    getUcenikIzostanci: async (jmbag) => {
        return await db.getUcenikIzostanci(jmbag);
    },
    getOcjeneUcenik: async (jmbag) => {
        return await db.getOcjeneUcenik(jmbag);
    },
    getUcenici: async () => {
        return await db.getUcenici();
    }
}

module.exports = homeDataLayerMethods;