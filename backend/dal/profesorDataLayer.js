const db = require("../db/postgresql.js");


const profesorDataLayerMethods = {
    getNastavnikPodaci: async (id) => {
        return await db.getNastavnikPodaci(id);
    },
    createIspit: async (data) => {
        return await db.createIspit(data.ocjena, data.id, data.datum, data.sifpredmet);
    },
    updateIspit: async (data) => {
        return await db.updateIspit(data.ocjena, data.id, data.datum, data.sifpredmet);
    },
    deleteIspit: async (data) => {
        return await db.deleteIspit(data.id, data.datum, data.sifpredmet);
    },
    createEvidencija: async (data) => {
        return await db.createEvidencija(data.id, data.datum);
    },
    getUcenikIzostanci: async (jmbag) => {
        return await db.getUcenikIzostanci(jmbag);
    },
    createBiljeska: async (data) => {
        return await db.createBiljeska(data.opis, data.id, data.sifpredmet);
    },
    updateUcenik: async (data) => {
        return await db.updateUcenik(data.ime, data.prezime, data.jmbag, data.sifrazred);
    },
    deleteBiljeska: async (jmbag) => {
        return await db.deleteBiljeska(jmbag);
    },
    deleteEvidencija: async (jmbag) => {
        return await db.deleteEvidencija(jmbag);
    },
    deleteIspitUcenik: async (jmbag) => {
        return await db.deleteIspitUcenik(jmbag);
    },
    deleteUpisanPredmet: async (jmbag) => {
        return await db.deleteUpisanPredmet(jmbag);
    },
    deleteUcenik: async (jmbag) => {
        return await db.deleteUcenik(jmbag);
    },
    getOdabraniUcenik: async (data) => {
        return await db.getOdabraniUcenik(data.id, data.sifpredmet);
    },
    getOcjene: async (data) => {
        return await db.getOcjene(data.sifpredmet, data.sifrazred);
    },
    deleteTestBiljeska: async (opis) => {
        return await db.deleteTestBiljeska(opis);
    }

}

module.exports = profesorDataLayerMethods;