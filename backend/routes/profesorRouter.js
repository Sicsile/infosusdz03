var express = require('express');
var router = express.Router();

const profesorService = require('../services/profesorService.js');

//dohvat svih podataka o nastavniku
router.get('/:id', async (req, res) => {
    try {
        const result = await profesorService.getNastavnikPodaci(req.params.id);
        if (result.status === 200) {
            res.json({
                user: result.user
            })
        } else {
            res.status(400).send("Traženi profesor ne postoji");
        }
    } catch (error) {
        res.status(400).send("Traženi profesor ne postoji");
    }
});

//kreiranje ispita
router.post('/odabrani-ucenik', async (req, res) => {
    try {
        const result = await profesorService.createIspit(req.body);
        if (result.status === 201) {
            res.status(201).send("OK");
        } else {
            res.status(400).send("NOTOK");
        }
    } catch (error) {
        res.status(400).send("NOTOK");
    }
});

//ažuriranje ocjene na ispitu
router.put('/odabrani-ucenik', async (req, res) => {
    try {
        const result = await profesorService.updateIspit(req.body);
        if (result.status === 200) {
            res.status(200).send("OK");
        } else {
            res.status(400).send("NOTOK");
        }
    } catch (error) {
        res.status(400).send("NOTOK");
    }
});

//brisanje ispita
router.delete('/odabrani-ucenik', async (req, res) => {
    try {
        const result = await profesorService.deleteIspit(req.query);
        if (result.status === 200) {
            res.status(200).send("OK");
        } else {
            res.status(400).send("NOTOK");
        }
    } catch (error) {
        res.status(400).send("NOTOK");
    }
});

//kreiranje izostanka
router.post('/odabrani-ucenik/evidencija', async (req, res) => {
    try {
        const result = await profesorService.createEvidencija(req.body);
        if (result.status === 201) {
            res.status(201).send("OK");
        } else {
            res.status(400).send("NOTOK");
        }
    } catch (error) {
        res.status(400).send("NOTOK");
    }
});

//kreiranje bilješke (složeno pravilo)
router.post('/odabrani-ucenik/biljeska', async (req, res) => {
    try {
        const result = await profesorService.createBiljeska(req.body);
        if (result.status === 201) {
            res.status(201).send(result.message);
        } else {
            res.status(400).send(result.message);
        }
    } catch (error) {
        res.status(400).send(result.message);
    }
});

//ažuriranje podataka o učeniku
router.post('/odabrani-ucenik/update', async (req, res) => {
    try {
        const result = await profesorService.updateUcenik(req.body);
        if (result.status === 200) {
            res.status(200).send("OK");
        } else {
            res.status(400).send("NOTOK");
        }
    } catch (error) {
        res.status(400).send("NOTOK");
    }
});

//brisanje učenika
router.delete('/odabrani-ucenik/:jmbag', async (req, res) => {
    try {
        const result = await profesorService.deleteUcenik(req.params.jmbag);
        if (result.status === 200) {
            res.status(200).send("Uspješno brisanje");
        } else {
            res.status(400).send("Brisanje učenika nije uspjelo");
        }
    } catch (error) {
        res.status(400).send("Brisanje učenika nije uspjelo");
    }
});

//dohvat podataka o ispitima za pojedinog učenika
router.get('/odabrani-ucenik/:id/:sifpredmet', async (req, res) => {
    try {
        const result = await profesorService.getOdabraniUcenik(req.params);
        if (result.status === 200) {
            res.json({
                ispiti: result.ispiti
            })
        } else {
            res.status(400).send("Ne postoji traženi učenik");
        }
    } catch (error) {
        res.status(400).send("Ne postoji traženi učenik");
    }
});

//dohvat ocjena i broja ocjena za pojedini razred za pojedini predmet
router.get('/statistika/:sifpredmet:sifrazred', async (req, res) => {
    try {
        const result = await profesorService.getOcjene(req.params);
        if (result.status === 200) {
            res.json({
                ocjene: result.ocjene
            })
        } else {
            res.status(400).send("Nema ocjena");
        }
    } catch (error) {
        res.status(400).send("Nema ocjena");
    }
});

router.delete('/odabrani-ucenik/biljeska/:opis', async (req, res) => {
    try {
        const result = await profesorService.deleteTestBiljeska(req.params.opis);
        if(result.status === 200){
            res.status(200).send("OK");
        } else {
            res.status(400).send("NOT OK");
        }
    } catch (error) {
        res.status(400).send("NOT OK");
    }
})


module.exports = router;