var express = require('express');
var router = express.Router();

const ucenikService = require('../services/ucenikService.js');


//dohvat svih podataka o učeniku
router.get('/:id', async (req, res) => {
    try {
        const result = await ucenikService.getUcenikPodaci(req.params.id);
        if(result.status === 200){
            res.json({
                user: result.user
            })
        } else {
            res.status(400).send("Traženi ucenik ne postoji");
        }
    } catch (error) {
        res.status(400).send("Traženi ucenik ne postoji");
    }
});

//dohvat upisanih predmeta za pojedinog učenika
router.get('/:id/upisani-predmeti', async (req, res) => {
    try {
        const result = await ucenikService.getUcenikPredmeti(req.params.id);
        if(result.status === 200){
            res.json({
                predmeti: result.predmeti
            })
        } else {
            res.status(400).send("Traženi ucenik ne postoji");
        }
    } catch (error) {
        res.status(400).send("Traženi ucenik ne postoji");
    }
});

//dohvat ocjena i broja ocjena za pojedinog učenika iz pojedinog predmeta
router.get('/statistika/:id/:sifpredmet', async (req, res) => {
    try {
        const result = await ucenikService.getOcjeneUcenikPredmet(req.params);
        if(result.status === 200){
            res.json({
                ocjene: result.ocjene
            })
        } else {
            res.status(400).send("Nema ocjena");
        }
    } catch (error) {
        res.status(400).send("Nema ocjena");
    }
})


module.exports = router;