var express = require('express');
var router = express.Router();
const jwt = require('jsonwebtoken');

const homeServiceMethods = require('../services/homeService.js')


//testiranje mocha
router.get('/', (req, res) => {
    res.status(200).send("OK");
})

//provjera kredencijala
router.post('/login', async (req, res) => {
    const body = {
        username: req.body.username,
        password: req.body.password
    }
    try {
        const result = await homeServiceMethods.homeServiceMethods.postLogin(body);
        if (result.status === 200) {
            res.json(result)
        } else {
            res.status(result.status).send("NOT OK");
        }
    } catch (err) {
        res.status(result.status).send("NOT OK")
    }
});

//dohvat svih razreda
router.get('/razredi', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.getRazredi();
        if (result.status === 200) {
            res.json(result)
        } else {
            res.status(400).send("Ne postoje razredi");
        }
    } catch (err) {
        console.log(err);
    }
});

//kreiranje razreda
router.post("/razred", async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.postRazred(req.body);
        if (result.status === 201) {
            res.status(201).send("Uspješno dodan razred");
        } else {
            res.status(400).send("Razred nije dodan");
        }
    } catch (err) {
        res.status(400).send("Razred nije dodan");
    }
})

//mijenjanje naziva razredu
router.put("/razred", async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.putRazred(req.body)
        if (result.status === 204) return res.status(204).send("Uspješno promijenjen naziv razreda");
        else return res.status(400).send("Izmjena naziv nije uspješna");
    } catch (err) {
        res.status(400).send("Izmjena naziv nije uspješna");
    }
})

//brisanje razreda
router.delete("/razred/:sifrazred", async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.deleteRazred(req.params.sifrazred);
        if (result.status === 200) {
            res.status(200).send("Uspješno obrisan razred");
        } else {
            res.status(400).send("Razred nije obrisan");
        }
    } catch (error) {
        res.status(400).send("Razred nije obrisan");
    }
})

//dohvat postanskih brojeva
router.get('/mjesta', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.getMjesta();
        if (result.status === 200) {
            res.json({
                mjesta: result.mjesta
            })
        } else {
            res.send("Ne postoje mjesta");
        }
    } catch (error) {
        res.send("Ne postoje mjesta");
    }
});

//kreiranje novog ucenika
router.post('/dodavanje', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.postUcenik(req.body);
        if(result.status === 201){
            res.status(201).send("Uspješno dodavanje");
        } else {
            res.status(400).send("Dodavanje nije uspjelo");
        }
    } catch (error) {
        res.status(400).send("Dodavanje nije uspjelo");
    }
});

//dohvat bilješki za pojedinog učenika
router.get('/ucenik:id/biljeske', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.getUcenikBiljeske(req.params.id);
        if(result.status === 200){
            res.json({
                biljeske: result.biljeske
            })
        } else {
            res.status(400).send("Za traženog učenika ne postoje bilješke");
        }
    } catch (error) {
        res.status(400).send("Za traženog učenika ne postoje bilješke");
    }
})

//dohvat izostanaka za pojedinog učenika
router.get('/ucenik:id/izostanci', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.getUcenikIzostanci(req.params.id);
        if(result.status === 200){
            res.json({
                izostanci: result.izostanci
            })
        } else {
            res.status(400).send("Za traženog učenika ne postoje zabilježeni izostanci");
        }
    } catch (error) {
        res.status(400).send("Za traženog učenika ne postoje zabilježeni izostanci");
    }
})

//dohvat ocjena za pojedinog učenika
router.get('/ucenik:id', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.getOcjeneUcenik(req.params.id);
        if(result.status === 200){
            res.json({
                ocjene: result.ocjene
            })
        } else {
            res.status(400).send("Ocjene ne postoje za traženog učenika");
        }
    } catch (error) {
        res.status(400).send("Ocjene ne postoje za traženog učenika");
    }
})

//dohvat svih učenika
router.get('/ucenici', async (req, res) => {
    try {
        const result = await homeServiceMethods.homeServiceMethods.getUcenici();
        if(result.status === 200){
            res.json({
                ucenici: result.ucenici
            })
        } else {
            res.status(400).send("Ne postoje ucenici")
        }
    } catch (error) {
        res.status(400).send("Ne postoje ucenici")
    }
});



//provjera kredencijala
function checkCred(res, password, username, method) {
    method(password).then((result) => {
        if (result !== undefined) {
            var usernameDB = result.ime + result.prezime;
            if (username === usernameDB) {
                const token = jwt.sign(result, process.env.SECRET_KEY);
                res.json({
                    token,
                    user: result
                })
            } else {
                res.status(403).send("NOT OK");
            }
        } else {
            res.status(403).send("NOT OK");
        }
    });
}


module.exports = router;