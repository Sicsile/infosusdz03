const jwt = require('jsonwebtoken');
const homeDataLayer = require('../dal/homeDataLayer');



//provjera kredencijala
const homeServiceMethods = {
    postLogin: async (body) => {
        const username = body.username;
        const password = body.password;
        if (password.length === 3) {
            const resultFromAwait = await homeDataLayer.getNastavnik(password)
            const result = checkCred(username, resultFromAwait);
            return result
        } else if (password.length === 6) {
            const resultFromAwait = await homeDataLayer.getUcenik(password)
            return checkCred(username, resultFromAwait);
        } else {
            return {
                status: 403
            }
        }
    },
    getRazredi: async () => {
        const resultFromAwait = await homeDataLayer.getRazredi();
        if (resultFromAwait !== undefined) {
            return {
                status: 200,
                razredi: resultFromAwait
            }
        } else {
            return {
                status: 400
            }
        }
    },
    postRazred: async (body) => {
        data = {
            sifrazred: body.sifrazred,
            naziv: body.naziv
        }
        const result = await homeDataLayer.createRazred(data);
        if(result != undefined) return {status: 201}
        else return {status: 400}
    },
    putRazred: async (body) => {
        data = {
            sifrazred: body.sifrazred,
            naziv: body.naziv
        }
        const result = await homeDataLayer.updateRazredNaziv(data);
        if(result != undefined) return {status: 204}
        else return {status: 400}
    },
    deleteRazred: async (sifrazred) => {
        const result = await homeDataLayer.deleteRazred(sifrazred);
        if(result != undefined) return {status: 200}
        else return {status: 400}
    },
    getMjesta: async () => {
        const result = await homeDataLayer.getMjesta();
        if(result != undefined){
            return {
                status: 200,
                mjesta: result
            }
        } else {
            return {status: 400}
        }
    },
    postUcenik: async (body) => {
        data = {
            ime: body.ime,
            prezime: body.prezime,
            razred: body.razred,
            jmbag: body.jmbag,
            pbr: body.pbr
        }
        const result = await homeDataLayer.createUcenik(data);
        if(result != undefined) return {status: 201}
        else return {status: 400}
    },
    getUcenikBiljeske: async (jmbag) => {
        const result = await homeDataLayer.getUcenikBiljeske(jmbag);
        if(result != undefined){
            return {
                status: 200,
                biljeske: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    getUcenikIzostanci: async (jmbag) => {
        const result = await homeDataLayer.getUcenikIzostanci(jmbag);
        if(result != undefined){
            return {
                status: 200,
                izostanci: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    getOcjeneUcenik: async (jmbag) => {
        const result = await homeDataLayer.getOcjeneUcenik(jmbag);
        if(result != undefined){
            return {
                status: 200,
                ocjene: result
            }
        } else {
            return{
                status: 400
            }
        }
    },
    getUcenici: async () => {
        const result = await homeDataLayer.getUcenici();
        if(result != undefined){
            return {
                status: 200,
                ucenici: result
            }
        } else {
            return {
                status: 400
            }
        }
    }
}



//provjera kredencijala
function checkCred(username, resultFromAwait) {
    if (resultFromAwait !== undefined) {
        var usernameDB = resultFromAwait.ime + resultFromAwait.prezime;
        if (username === usernameDB) {
            const token = jwt.sign(resultFromAwait, process.env.SECRET_KEY);
            return {
                status: 200,
                token,
                user: resultFromAwait
            }
        } else {
            return {
                status: 403
            }
        }
    } else {
        return {
            status: 403
        }
    }
}

module.exports = {
    homeServiceMethods,
    checkCred   
}