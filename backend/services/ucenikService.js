const ucenikDataLayer = require('../dal/ucenikDataLayer.js');


const ucenikServiceMethods = {
    getUcenikPodaci: async (jmbag) => {
        const result = await ucenikDataLayer.getUcenikPodaci(jmbag);
        if(result != undefined){
            return {
                status: 200,
                user: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    getUcenikPredmeti: async (jmbag) => {
        const result = await ucenikDataLayer.getUcenikPredmeti(jmbag);
        if(result != undefined){
            return {
                status: 200,
                predmeti: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    getOcjeneUcenikPredmet: async (params) => {
        data = {
            id: params.id,
            sifpredmet: params.sifpredmet
        }
        const result = await ucenikDataLayer.getOcjeneUcenikPredmet(data);
        if(result != undefined){
            return {
                status: 200,
                ocjene: result
            }
        } else {
            return {
                status: 400
            }
        }
    }
}

module.exports = ucenikServiceMethods;