const profesorDataLayer = require('../dal/profesorDataLayer.js');

const profesorServiceMethods = {
    getNastavnikPodaci: async (id) => {
        const result = await profesorDataLayer.getNastavnikPodaci(id);
        if (result != undefined) {
            return {
                status: 200,
                user: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    createIspit: async (body) => {
        data = {
            ocjena: body.ocjena,
            id: body.id,
            datum: body.datum,
            sifpredmet: body.sifpredmet
        }
        const result = await profesorDataLayer.createIspit(data);
        if (result === 1) return {
            status: 201
        }
        else return {
            status: 400
        }
    },
    updateIspit: async (body) => {
        data = {
            ocjena: body.ocjena,
            id: body.id,
            datum: body.datum,
            sifpredmet: body.sifpredmet
        }
        const result = await profesorDataLayer.updateIspit(data);
        if (result === 1) return {
            status: 200
        }
        else return {
            status: 400
        }
    },
    deleteIspit: async (params) => {
        data = {
            id: params.id,
            datum: params.datum,
            sifpredmet: params.sifpredmet
        }
        const result = await profesorDataLayer.deleteIspit(data);
        if (result === 1) return {
            status: 200
        }
        else return {
            status: 400
        }
    },
    createEvidencija: async (body) => {
        data = {
            id: body.id,
            datum: body.datum
        }
        const result = await profesorDataLayer.createEvidencija(data);
        if (result === 1) return {
            status: 201
        }
        else return {
            status: 400
        }
    },
    createBiljeska: async (body) => {
        let ts = Date.now();
        let date_ob = new Date(ts);
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let currentDate = (year + "-" + month + "-" + date);

        var postojiIzostanak = false;
        var datumi = []

        data = {
            id: body.id,
            sifpredmet: body.sifpredmet,
            opis: body.opis
        }
        const result = await profesorDataLayer.getUcenikIzostanci(data.id);
        if (result != undefined) {
            result.forEach(res => {
                var year = res.datum.getFullYear().toString();
                var month = ("0" + (res.datum.getMonth() + 1)).slice(-2);
                var day = ("0" + res.datum.getDate()).slice(-2);
                var fullDate = year + "-" + month + "-" + day
                datumi.push(fullDate);
            });
            datumi.forEach(temp => {
                if (temp === currentDate) {
                    postojiIzostanak = true;
                }
            });
            if (postojiIzostanak) {
                return {
                    status: 400,
                    message: "Na današnji dan učenik je izostao s nastave te mu se ne može unjeti bilješka"
                }
            } else {
                const result2 = await profesorDataLayer.createBiljeska(data);
                if (result2 === 1) return {
                    status: 201,
                    message: "OK"
                }
                else return {
                    status: 400,
                    message: "NOT OK"
                }
            }
        }

    },
    updateUcenik: async (body) => {
        data = {
            ime: body.ime,
            prezime: body.prezime,
            jmbag: body.jmbag,
            sifrazred: body.sifrazred
        }
        const result = await profesorDataLayer.updateUcenik(data);
        if (result === 1) return {
            status: 200
        }
        else return {
            status: 400
        }
    },
    deleteUcenik: async (jmbag) => {
        try {
            // await profesorDataLayer.deleteBiljeska(jmbag);
            // await profesorDataLayer.deleteEvidencija(jmbag);
            // await profesorDataLayer.deleteIspitUcenik(jmbag);
            // await profesorDataLayer.deleteUpisanPredmet(jmbag);
            await profesorDataLayer.deleteUcenik(jmbag);
            return {
                status: 200
            }
        } catch (error) {
            return {
                staus: 400
            }
        }
    },
    getOdabraniUcenik: async (params) => {
        data = {
            id: params.id,
            sifpredmet: params.sifpredmet
        }
        const result = await profesorDataLayer.getOdabraniUcenik(data);
        if (result != undefined) {
            return {
                status: 200,
                ispiti: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    getOcjene: async (params) => {
        data = {
            sifpredmet: params.sifpredmet,
            sifrazred: params.sifrazred
        }
        const result = await profesorDataLayer.getOcjene(data);
        if(result != undefined){
            return {
                status: 200,
                ocjene: result
            }
        } else {
            return {
                status: 400
            }
        }
    },
    deleteTestBiljeska: async (opis) => {
        const result = await profesorDataLayer.deleteTestBiljeska(opis);
        if(result != undefined){
            return {
                status: 200
            }
        } else {
            return {
                status: 400
            }
        }
    }
}

module.exports = profesorServiceMethods;