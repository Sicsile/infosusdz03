const {
    expect, should
} = require('chai');
const assert = require('chai');
const assertHttp = require('chai-http');
const app = require('../server.js');
assert.should();
assert.use(assertHttp);

const homeServiceMethods = require('../services/homeService.js');
const homeDataLayerMethods = require('../dal/homeDataLayer.js');


describe('Testiranje login', () => {
    const credentialsValid = {
        'username': 'DaliborJuran',
        'password': '100'
    }
    const credentialsInvalid = {
        'username': 'DaliborJuran',
        'password': 'xxx'
    }

    describe('POST /login', () => {
        it('Treba vratit status kod 200 ako su kredencijali dobri', done => {
            assert.request(app)
                .post('/login')
                .send(credentialsValid)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });

        it('Treba vratit status kod 403 ako kredencijali nisu dobri', done => {
            assert.request(app)
                .post('/login')
                .send(credentialsInvalid)
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });

        it('U odgovoru treba sadržavati objekt koji ima ime koje je jednako Dalibor', done => {
            assert.request(app)
                .post('/login')
                .send(credentialsValid)
                .end((err, res) => {
                    res.body.user.ime.should.equal("Dalibor");
                    done();
                });
        });

        it('U odgovoru treba sadržavati objekt token', done => {
            assert.request(app)
                .post('/login')
                .send(credentialsValid)
                .end((err, res) => {
                    res.body.should.have.property('token');
                    done();
                });
        });

    });
});

describe('Testiranje učenika', () => {
    describe('GET /ucenik/:id/upisani-predmeti', () => {
        it('Treba vratiti listu upisanih predmeta', done => {
            assert.request(app)
                .get('/ucenik/000000/upisani-predmeti')
                .end((err, res) => {
                    expect(res.body.predmeti).to.be.an('array');
                    done();
                })
        })
    });

    describe('GET /ucenik/:id', () => {
        it('Treba vratiti sve podatke o uceniku', done => {
            assert.request(app)
                .get('/ucenik/000000/')
                .end((err, res) => {
                    res.body.user[0].should.have.property('pbr');
                    res.body.user[0].should.have.property('ime');
                    res.body.user[0].should.have.property('prezime');
                    res.body.user[0].should.have.property('nazivmjesto');
                    res.body.user[0].should.have.property('nazivrazred');
                    res.body.user[0].should.have.property('nazivskola');
                    done();
                })
        })
    });
})

describe('Testiranje bilješki', () => {
    const biljeskaValid = {
        'id': '821323',
        'sifpredmet': '3',
        'opis': 'mocha testing biljeska'
    }

    const biljeskaInvalid = {
        'id': '821323',
        'opis': 'mocha testing biljeska'
    }

    describe('POST /profesor/odabrani-ucenik/biljeska', () => {
        it('Ako nisu zadani svi parametri, treba vratiti 400', done => {
            assert.request(app)
                .post('/profesor/odabrani-ucenik/biljeska')
                .send(biljeskaInvalid)
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                })
        })

    })

    describe('POST /profesor/odabrani-ucenik/biljeska', () => {
        it('Ako je bilješka uspješno kreirana treba se dobiti status kod 201', done => {
            assert.request(app)
                .post('/profesor/odabrani-ucenik/biljeska')
                .send(biljeskaValid)
                .end((err, res) => {
                    res.should.have.status(201);
                    done();
                })
        })

    })

    describe('GET /ucenik:821323/biljeske', () => {
        it('Treba vratiti novokreiranu bilješku', done => {
            assert.request(app)
                .get('/ucenik' + biljeskaValid.id + '/biljeske')
                .end((err, res) => {
                    res.body.should.have.property('biljeske');
                    res.body.biljeske[res.body.biljeske.length - 1].opis.should.equal(biljeskaValid.opis);
                    done();
                })
        })
    })

    describe('DELETE /profesor/odabrani-ucenik/biljeska', () => {
        it('Treba izbrisati novokreiranu bilješku', done => {
            assert.request(app)
                .delete('/profesor/odabrani-ucenik/biljeska/' + biljeskaValid.opis)
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                })
        })
    })

})

//unit poslovni sloj
describe('Test checkCred method', () => {
    var objectValid = homeServiceMethods.checkCred('DaliborJuran', {ime: 'Dalibor', prezime: 'Juran'});
    var objectInvalid = homeServiceMethods.checkCred('DaliborJuran', {ime: 'x', prezime: 'x'});
    
    it('Treba vratiti objekt s određenim svojstvima', done => {
        expect(objectValid.status).to.equal(200);
        expect(objectValid).to.have.property('status');
        expect(objectValid).to.have.property('token');
        expect(objectValid).to.have.property('user');             
        done();
    });

    it('Treba vratiti objekt sa statusom 403', done => {
        expect(objectInvalid.status).to.equal(403);
        done();
    });

    
})

//unit 
describe('Testiranje dohvaćanja mjesta', () => {
    it('Treba vratiti objekte koji imaju pbr i naziv', async () => {
        var object = await homeDataLayerMethods.getMjesta();
        if(object != undefined){
            expect(object[0]).to.have.property('pbr');
            expect(object[0]).to.have.property('naziv');  
            expect(Object.keys(object[0]).length).to.equal(2);
        } else {
            expect(object).to.be.an('undefined');
        }
    })
})