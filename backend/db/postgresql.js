const Pool = require('pg').Pool;
require('dotenv').config();

const pool = new Pool({
    user: process.env.USER,
    database: process.env.DATABASE,
    password: process.env.PASSWORD,
    port: process.env.PORTDB,
    host: process.env.HOST,
});



//dohvat svih razreda
const getRazredi = async function () {
    try {
        var result = await pool.query(`select * from razred`);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//dohvat jednog nastavnika
const getNastavnik = async function (id) {
    try {
        var result = await pool.query(`select *
        from nastavnik
        where sifnastavnik = $1`, [id]);
        return result.rows[0];

    } catch (error) {
        console.log(error);
    }
}

//dohvat jednog učenika
const getUcenik = async function (id) {
    try {
        var result = await pool.query(`select *
        from ucenik
        where jmbag = $1`, [id]);
        return result.rows[0];

    } catch (error) {
        console.log(error);
    }
}

//kreiraj razred
const createRazred = async function(sifrazred, naziv){
    try {
        var result = await pool.query(`INSERT INTO razred
        VALUES($1, $2, '1004');`, [sifrazred, naziv]);
        return result.rowCount;
    } catch (error) {
        console.log(error);
    }
}

//mijnjanje naziva razredu
const updateRazredNaziv = async function(sifrazred, naziv){
    try{    
        var result = await pool.query(`update razred
        set naziv = $2
        where sifrazred = $1`, [sifrazred, naziv]);
        return result.rowCount;
    } catch(err) {
        console.log(err);
    }
}

//brisanje razreda
const deleteRazred = async function(sifrazred){
    try {
        var result = await pool.query(`delete from razred
        where sifrazred = $1`, [sifrazred]);
        return result.rowCount;
    } catch (error) {
        console.log(error);
    }
}

//dohvat postanskih brojeva
const getMjesta = async function () {
    try {
        var result = await pool.query(`select * from mjesto`);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//kreiranje novog ucenika i upisivanje predmeta
const createUcenik = async function(ime, prezime, razred, jmbag, pbr){
    try {
        var result = await pool.query(`insert into ucenik (ime, prezime, sifrazred, jmbag, pbr, uloga)        
        values ($1, $2, $3, $4, $5, 'ucenik')`, [ime, prezime, razred, jmbag, pbr]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '1')`, [jmbag]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '2')`, [jmbag]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '3')`, [jmbag]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '4')`, [jmbag]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '5')`, [jmbag]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '6')`, [jmbag]);
        result = await pool.query(`insert into upisan_predmet
        values($1, '7')`, [jmbag]);
        
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//dohvat bilješki za pojedinog učenika
const getUcenikBiljeske = async function (id) {
    try {
        var result = await pool.query(`select opis, sifpredmet
        from ucenik natural join biljeska natural join predmet
        where jmbag = $1`, [id]);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//dohvat ocjena za pojedinog učenika
const getOcjeneUcenik = async function(id) {
    try {
        var result = await pool.query(`select datum::date, ocjena, naziv, sifpredmet
        from ucenik natural join ispit
                    natural join predmet
        where ucenik.jmbag = $1
        order by ispit.datum desc`, [id]);
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

//dohvat svih ucenika
const getUcenici = async function () {
    try {
        var result = await pool.query(`select * from ucenik natural join razred`)
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}


//---------------------------------------------------------------------------


//dohvat svih podataka o nastavniku
const getNastavnikPodaci = async function (id) {
    try {
        var result = await pool.query(`select pbr, ime, prezime, predmet.naziv as nazivpredmet, 
        mjesto.naziv as nazivmjesto, razred.naziv as nazivrazred, skola.naziv as nazivskola
 from nastavnik natural join mjesto
                join predmet on nastavnik.sifpredmet = predmet.sifpredmet
                join razred on nastavnik.sifrazred = razred.sifrazred
                join skola on razred.sifskola = skola.sifskola
 where sifnastavnik = $1`, [id]);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//kreiranje ispita
const createIspit = async function(ocjena, id, datum, sifpredmet){
    try {
        var result = await pool.query(`insert into ispit        
        values ($1, $2, $3, $4)`, [ocjena, datum, id, sifpredmet]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//ažuriranje ocjene na ispitu
const updateIspit = async function(ocjena, id, datum, sifpredmet){
    try {
        var result = await pool.query(`update ispit
        set ocjena = $1
        where jmbag = $2 and datum = $3 and sifpredmet = $4`, [ocjena, id, datum, sifpredmet]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//brisanje ispita
const deleteIspit = async function(id, datum, sifpredmet){
    try {
        var result = await pool.query(`delete from ispit
        where jmbag = $1 and datum = $2 and sifpredmet = $3`, [id, datum, sifpredmet]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//kreiranje izostanka
const createEvidencija = async function(id, datum){
    try {
        var result = await pool.query(`insert into evidencija        
        values ('0', $1, $2)`, [datum, id]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//dohvat svih izostanaka za jednog ucenika
const getUcenikIzostanci = async function (id) {
    try {
        var result = await pool.query(`select *
        from ucenik natural join evidencija
        where jmbag = $1`, [id]);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//kreiranje bilješke
const createBiljeska = async function(opis, id, sifpredmet){
    try {
        var result = await pool.query(`insert into biljeska(opis, jmbag, sifpredmet)        
        values ($1, $2, $3)`, [opis, id, sifpredmet]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//ažuriranje podataka o učeniku
const updateUcenik = async function(ime, prezime, jmbag, sifrazred){
    try {
        var result = await pool.query(`update ucenik set(ime, prezime, sifrazred) = ($1, $2, $4)
        where jmbag = $3`, [ime, prezime, jmbag, sifrazred]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//brisanje bilješki
const deleteBiljeska = async function(jmbag){
    try {
        var result = await pool.query(`delete from biljeska
        where jmbag = $1`, [jmbag]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//brisanje testing bilješke
const deleteTestBiljeska = async function(opis){
    try {
        var result = await pool.query(`delete from biljeska
        where opis = $1`, [opis]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}



//brisanje evidencija
const deleteEvidencija = async function(jmbag){
    try {
        var result = await pool.query(`delete from evidencija
        where jmbag = $1`, [jmbag]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//brisanje ispita za pojedniog učenika
const deleteIspitUcenik = async function(jmbag){
    try {
        var result = await pool.query(`delete from ispit
        where jmbag = $1`, [jmbag]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//brisanje upisanih predmeta za pojedinog učenika
const deleteUpisanPredmet = async function(jmbag){
    try {
        var result = await pool.query(`delete from upisan_predmet
        where jmbag = $1`, [jmbag]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//brisanje učenika
const deleteUcenik = async function(jmbag){
    try {
        var result = await pool.query(`delete from ucenik
        where jmbag = $1`, [jmbag]);
        return result.rowCount
    } catch (error) {
        console.log(error)
    }
}

//dohvat podataka o ispitima za pojedinog učenika
const getOdabraniUcenik = async function (id, sifpredmet) {
    try {
        var result = await pool.query(`select ime, prezime, jmbag, datum::text, ocjena, naziv
        from ucenik natural join ispit natural join predmet
        where jmbag = $1 and sifpredmet = $2
        order by datum desc`, [id, sifpredmet]);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//dohvat ocjena i broja ocjena za pojedini razred za pojedini predmet
const getOcjene = async function(sifpredmet, sifrazred) {
    try {
        var result = await pool.query(`select ocjena, count(ocjena) as cnt
        from ucenik join ispit on ucenik.jmbag = ispit.jmbag 
                    join predmet on ispit.sifpredmet = predmet.sifpredmet
        where ispit.sifpredmet = $1 and ucenik.sifrazred = $2
        group by ocjena`, [sifpredmet, sifrazred]);
        return result.rows
    } catch (error) {
        console.log(error)
    }
}


//-----------------------------------------------------------------------


//dohvat svih podataka o uceniku
const getUcenikPodaci = async function (id) {
    try {
        var result = await pool.query(`select pbr, ime, prezime, mjesto.naziv as nazivmjesto,
        razred.naziv as nazivrazred, skola.naziv as nazivskola
 from ucenik natural join mjesto
             join razred on ucenik.sifrazred = razred.sifrazred
             join skola on razred.sifskola = skola.sifskola
 where jmbag = $1`, [id]);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//dohvat upisanih predmeta za pojedinog ucenika
const getUcenikPredmeti = async function (id) {
    try {
        var result = await pool.query(`select *
        from ucenik natural join upisan_predmet natural join predmet
        where jmbag = $1`, [id]);
        return result.rows;
    } catch (error) {
        console.log(error);
    }
}

//dohvat ocjena i broja ocjena za pojedinog učenika iz pojedinog predmeta
const getOcjeneUcenikPredmet = async function(id, sifpredmet) {
    try {
        var result = await pool.query(`select ocjena, count(ocjena) as cnt
        from ucenik natural join ispit
                    natural join predmet
        where ucenik.jmbag = $1 and sifpredmet = $2
		group by ocjena`, [id, sifpredmet]);
        return result.rows
    } catch (error) {
        console.log(error)
    }
}



module.exports = {
    getNastavnik,
    getNastavnikPodaci,
    getUcenik,
    getUcenikPodaci,
    getUcenici,
    getOcjene,
    getUcenikPredmeti,
    getOcjeneUcenik,
    getOcjeneUcenikPredmet,
    getUcenikBiljeske,
    getOdabraniUcenik,
    getUcenikIzostanci,
    deleteIspit,
    updateIspit,
    createIspit,
    createEvidencija,
    createBiljeska,
    getRazredi,
    updateUcenik,
    deleteBiljeska,
    deleteTestBiljeska,
    deleteEvidencija,
    deleteIspitUcenik,
    deleteUpisanPredmet,
    deleteUcenik,
    getMjesta,
    createUcenik,
    updateRazredNaziv,
    createRazred,
    deleteRazred
}