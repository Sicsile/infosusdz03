const express = require('express');
const app = express();
require('dotenv').config(); //environment variables
const cors = require('cors'); // omogućavanje zahtjeva s frontenda
var corsOptions = {
    origin: ['http://localhost:8080', 'http://127.0.0.1:8080', 'http://127.0.0.1:5432', 'http://127.0.0.1:5433', 'http://localhost:5432', 'http://localhost:5433'],
}
app.use(cors(corsOptions));
var bodyParser = require('body-parser'); //parsiranje tijela zahtjeva
app.use(bodyParser.json())


//routes
const homeRouter = require('./routes/homeRouter.js');
const profesorRouter = require('./routes/profesorRouter.js');
const ucenikRouter = require('./routes/ucenikRouter.js');
app.use("/", homeRouter);
app.use("/profesor", profesorRouter);
app.use("/ucenik", ucenikRouter)


const port = process.env.PORTSERVER;
app.listen(port, () => {
    console.log(`Slušanje na http://127.0.0.1:${port}`);
});

module.exports = app;